package de.summerfeeling.betterscreenshot;

import de.summerfeeling.betterscreenshot.publisher.Publisher;
import de.summerfeeling.betterscreenshot.publisher.Result;
import de.summerfeeling.betterscreenshot.utils.BetterDropDownElement;
import de.summerfeeling.betterscreenshot.utils.DescribableBooleanElement;
import de.summerfeeling.betterscreenshot.utils.DescribableHeaderElement;
import net.labymod.api.LabyModAddon;
import net.labymod.api.events.MessageSendEvent;
import net.labymod.core.LabyModCore;
import net.labymod.gui.elements.DropDownMenu;
import net.labymod.main.LabyMod;
import net.labymod.settings.elements.ControlElement.IconData;
import net.labymod.settings.elements.HeaderElement;
import net.labymod.settings.elements.SettingsElement;
import net.labymod.settings.elements.StringElement;
import net.labymod.utils.Material;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.event.ClickEvent;
import net.minecraft.event.ClickEvent.Action;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.ChatStyle;
import org.lwjgl.input.Keyboard;

import java.awt.*;
import java.awt.datatransfer.StringSelection;
import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

public class BetterScreenshot extends LabyModAddon {
	
	public static final File SCREENSHOT_FOLDER = new File(Minecraft.getMinecraft().mcDataDir, "screenshots");
	public static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd_HH.mm.ss");
	public static final ExecutorService ASYNC_POOL = Executors.newCachedThreadPool();
	
	private static ScheduledExecutorService SCHEDULER = Executors.newScheduledThreadPool(1);
	private static BetterScreenshot instance;
	
	private Resolution resolution = Resolution.ORIGINAL;
	private boolean enabled = true;
	
	private boolean fastScreenshots = false;
	private boolean copyToClipboard = false;
	private boolean directUpload = false;
	private boolean uploadEnabled = true;
	
	private boolean deleteEnabled = true;
	
	//
	private String apiKey;
	
	public void onEnable() {
		BetterScreenshot.instance = this;
		
		getApi().getEventManager().register((MessageSendEvent) message -> {
			if (message.startsWith("-uploadscreenshot") || message.startsWith("-tweetscreenshot") || message.startsWith("-deletescreenshot")) {
				EntityPlayerSP player = LabyModCore.getMinecraft().getPlayer();
				
				try {
					String filePath = message.substring(message.indexOf("\"") + 1, message.lastIndexOf("\""));
					File file = new File(filePath);
					
					if (!file.exists()) {
						getApi().displayMessageInChat("§cError: §fThe given file does not exists!");
						return true;
					}
					
					if (!file.canRead()) {
						getApi().displayMessageInChat("§cError: §fThe given file is not accessible!");
						return true;
					}
					
					if (message.startsWith("-uploadscreenshot")) {
						Publisher.publishScreenshot(new File(filePath), (result, detail) -> {
							if (result == Result.UPLOAD_DONE) {
								player.addChatMessage(new ChatComponentText("§aUpload finished: " + detail).setChatStyle(new ChatStyle().setChatClickEvent(new ClickEvent(Action.OPEN_URL, detail))));
								if (copyToClipboard) Toolkit.getDefaultToolkit().getSystemClipboard().setContents(new StringSelection(detail), new StringSelection(detail));
							} else {
								getApi().displayMessageInChat("§cUpload failed: §f" + detail);
								player.addChatMessage(new ChatComponentText("§cUpload failed: §f" + detail));
							}
						});
					} else if (message.startsWith("-deletescreenshot")) {
						file.delete();
						ChatHelper.deleteMessage(file);
						getApi().displayMessageInChat("§aScreenshot deleted!");
					}
				} catch (Exception e) {
					player.addChatMessage(new ChatComponentText("§cSomething went wrong! Did you execute the upload command yourself? (You must not, just hit " + Keyboard.getKeyName(Minecraft.getMinecraft().gameSettings.keyBindScreenshot.getKeyCode()) + "!)"));
				}
				
				return true;
			}
			
			return false;
		});
	}
	
	public void onDisable() {
	
	}
	
	public void loadConfig() {
		this.enabled = !getConfig().has("enabled") || getConfig().get("enabled").getAsBoolean();
		this.resolution = (!getConfig().has("resolution") || getConfig().get("resolution").isJsonNull() ? Resolution.ORIGINAL : Resolution.valueOf(getConfig().get("resolution").getAsString()));
		
		this.copyToClipboard = getConfig().has("copyToClipboard") && getConfig().get("copyToClipboard").getAsBoolean();
		this.uploadEnabled = !getConfig().has("uploadEnabled") || getConfig().get("uploadEnabled").getAsBoolean();
		this.directUpload = getConfig().has("directUpload") && getConfig().get("directUpload").getAsBoolean();
		this.fastScreenshots = getConfig().has("fastScreenshots") && getConfig().get("fastScreenshots").getAsBoolean();
		
		this.deleteEnabled = !getConfig().has("deleteEnabled") || getConfig().get("deleteEnabled").getAsBoolean();
	}
	
	protected void fillSettings(List<SettingsElement> list) {
		list.clear();
		
		StringElement apiKeyElement = new StringElement("API-Key", this, new IconData(Material.COMMAND), "apiKey", "");
		apiKeyElement.setVisible(false);
		
		list.add(new HeaderElement("§6BetterScreenshot §7» §bSummerfeeling"));
		list.add(new DescribableBooleanElement("Enabled", this, new IconData(Material.LEVER), "enabled", true).editDescriptionText("Whether this addon should be enabled or not"));
		
		list.add(new DescribableHeaderElement("Image Settings"));
		list.add(new BetterDropDownElement<Resolution>("Scale", new DropDownMenu<Resolution>("", 0, 0, 0, 0).fill(Resolution.values())).select(resolution).changeListener(resolution -> {
			getConfig().addProperty("resolution", resolution.name());
			loadConfig();
			
			System.out.println("resolution = " + resolution);
			
			if (resolution == Resolution.FULL_HD) {
				System.out.println("Showing key");
				apiKeyElement.setVisible(true);
			} else {
				System.out.println("Hiding key");
				apiKeyElement.setVisible(false);
			}
		}).entryDrawer((object, x, y, trimmed) -> {
			Resolution resolution = (Resolution) object;
			
			if (resolution == Resolution.ORIGINAL) {
				LabyMod.getInstance().getDrawUtils().drawString("Keep resolution", x, y);
			} else {
				LabyMod.getInstance().getDrawUtils().drawString(resolution.getWidth() + " x " + resolution.getHeight(), x, y);
			}
		}).editDescriptionText("Should the uploaded picture be scaled? (Useful to lower file size and therefore upload time, especially for displays over Full HD)").setIconData(new IconData("labymod/textures/settings/elements/default/gui_scaling.png")));

		list.add(new DescribableHeaderElement("§eimgur.com Upload").editDescriptionText("You can upload your screenshot with one click - or automatically - to imgur.com"));
		list.add(new DescribableBooleanElement("[UPLOAD] Enabled", this, new IconData(Material.LEVER), "uploadEnabled", true).editDescriptionText("Shows / hides the [UPLOAD] button"));
		list.add(new DescribableBooleanElement("Automatic copy to clipboard", this, new IconData(Material.BOOK_AND_QUILL), "copyToClipboard", false).editDescriptionText("Should the imgur.com link be automatically copied to your clipboard?"));
		list.add(new DescribableBooleanElement("Automatic upload", this, new IconData(Material.FIREWORK), "directUpload", false).editDescriptionText("Should every screenshot be uploaded automatically?"));
		list.add(new DescribableBooleanElement("Fast screenshots", this, new IconData(Material.SUGAR), "fastScreenshots", false).editDescriptionText("Don't freeze your game while taking a screenshot."));
		
		list.add(new DescribableHeaderElement("§cDelete Function").editDescriptionText("You can delete accidentally created screenshots with one click directly in Minecraft"));
		list.add(new DescribableBooleanElement("[DELETE] Enabled", this, new IconData(Material.LEVER), "deleteEnabled", true).editDescriptionText("Shows / hides the [DELETE] button"));
		
		list.add(new HeaderElement(" "));
		list.add(apiKeyElement);
		
		list.add(new HeaderElement(" "));
		list.add(new HeaderElement("§oUpload limited to 500 pictures a day due to imgur's rate limit."));
	}
	
	public static BetterScreenshot getInstance() {
		return instance;
	}
	
	public boolean isEnabled() {
		return enabled;
	}
	
	public Resolution getResolution() {
		return resolution;
	}
	
	public boolean isDirectUpload() {
		return directUpload;
	}
	
	public boolean isCopyToClipboard() {
		return copyToClipboard;
	}
	
	public boolean isUploadEnabled() {
		return uploadEnabled;
	}
	
	public boolean isDeleteEnabled() {
		return deleteEnabled;
	}
	
	public boolean isFastScreenshots() {
		return fastScreenshots;
	}
	
	@Override
	public String toString() {
		return "BetterScreenshot{" +
				"enabled=" + enabled +
				", resolution=" + resolution +
				", copyToClipboard=" + copyToClipboard +
				", directUpload=" + directUpload +
				", uploadEnabled=" + uploadEnabled +
				", deleteEnabled=" + deleteEnabled +
				'}';
	}
}
