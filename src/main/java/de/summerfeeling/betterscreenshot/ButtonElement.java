package de.summerfeeling.betterscreenshot;

import net.labymod.core.LabyModCore;
import net.labymod.settings.elements.ControlElement;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;

public class ButtonElement extends ControlElement {
	
	private Runnable toggleListener;
	private Runnable callback;
	
	private GuiButton runButton;
	private String buttonText;
	
	private boolean enabled = true;
	
	public ButtonElement(String displayName, IconData iconData, String buttonText) {
		super(displayName, null, iconData);
		
		this.buttonText = buttonText;
		this.createButton();
	}
	
	public ButtonElement(String displayName, IconData iconData, String buttonText, Runnable toggleListener) {
		super(displayName, null, iconData);
		
		this.toggleListener = toggleListener;
		this.buttonText = buttonText;
		this.createButton();
	}
	
	public void createButton() {
		this.runButton = new GuiButton(-2, 0, 0, 0, 20, buttonText != null && !buttonText.isEmpty() ? buttonText : "");
	}
	
	@Override
	public void draw(int x, int y, int maxX, int maxY, int mouseX, int mouseY) {
		super.draw(x, y, maxX, maxY, mouseX, mouseY);
		int width = getObjectWidth();
		
		if (runButton != null) {
			LabyModCore.getMinecraft().setButtonXPosition(runButton, maxX - width - 2);
			LabyModCore.getMinecraft().setButtonYPosition(runButton, y + 1);
			
			this.runButton.enabled = enabled;
			this.runButton.setWidth(width);
			
			LabyModCore.getMinecraft().drawButton(runButton, mouseX, mouseY);
		}
	}
	
	@Override
	public void mouseClicked(int mouseX, int mouseY, int mouseButton) {
		super.mouseClicked(mouseX, mouseY, mouseButton);
	
		if (runButton.mousePressed(Minecraft.getMinecraft(), mouseX, mouseY) && enabled) {
			if (toggleListener != null) toggleListener.run();
			if (callback != null) callback.run();
			
			this.runButton.playPressSound(this.mc.getSoundHandler());
		}
	}
	
	public ButtonElement addCallback(Runnable callback) {
		this.callback = callback;
		return this;
	}
	
	public boolean isEnabled() {
		return enabled;
	}
	
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
}
