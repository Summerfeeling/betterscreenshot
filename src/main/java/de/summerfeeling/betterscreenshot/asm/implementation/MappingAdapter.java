package de.summerfeeling.betterscreenshot.asm.implementation;

public abstract class MappingAdapter {
	
	private boolean forgeClient;
	
	public MappingAdapter(boolean forgeClient) {
		this.forgeClient = forgeClient;
	}
	
	public boolean isForgeClient() {
		return forgeClient;
	}
	
	public String getHelperClassName() {
		if (forgeClient) return getForgeHelperClassName();
		return getObfuscatedHelperClassName();
	}
	
	public abstract String getObfuscatedHelperClassName();
	public abstract String getForgeHelperClassName();

	public abstract String getMethodMapping(String original);
	
}
