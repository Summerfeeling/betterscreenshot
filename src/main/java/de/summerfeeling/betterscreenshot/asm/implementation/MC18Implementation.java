package de.summerfeeling.betterscreenshot.asm.implementation;

import java.util.HashMap;
import java.util.Map;

public class MC18Implementation extends MappingAdapter {
	
	private Map<String, String> vanillaMappings = new HashMap<String, String>() {{
		put("saveScreenshot§(Ljava/io/File;IILbfw;)Leu;", "a");
		put("saveScreenshot§(Ljava/io/File;Ljava/lang/String;IILbfw;)Leu;", "a");
		put("getTimestampedPNGFileForDirectory§(Ljava/io/File;)Ljava/io/File;", "a");
	}};
	
	private Map<String, String> forgeMappings = new HashMap<String, String>() {{
		put("saveScreenshot§(Ljava/io/File;IILbfw;)Leu;", "func_148260_a");
		put("saveScreenshot§(Ljava/io/File;Ljava/lang/String;IILbfw;)Leu;", "func_148259_a");
		put("getTimestampedPNGFileForDirectory§(Ljava/io/File;)Ljava/io/File;", "func_74290_a");
	}};
	
	public MC18Implementation(boolean forgeClient) {
		super(forgeClient);
	}
	
	@Override
	public String getObfuscatedHelperClassName() {
		return "avj";
	}
	
	@Override
	public String getForgeHelperClassName() {
		return "net.minecraft.util.ScreenShotHelper";
	}
	
	@Override
	public String getMethodMapping(String original) {
		return isForgeClient() ? forgeMappings.get(original) : vanillaMappings.get(original);
	}
	
	@Override
	public String toString() {
		return getClass().getSimpleName() + "{" +
				"forgeClient=" + isForgeClient() +
				",helperClass=" + getHelperClassName() +
				'}';
	}
}
