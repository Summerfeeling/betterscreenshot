package de.summerfeeling.betterscreenshot.asm.implementation;

public class UnobfuscatedImplementation extends MappingAdapter {
	
	public UnobfuscatedImplementation(boolean forgeClient) {
		super(forgeClient);
	}
	
	@Override
	public String getObfuscatedHelperClassName() {
		return "net.minecraft.util.ScreenShotHelper";
	}
	
	@Override
	public String getForgeHelperClassName() {
		return "net.minecraft.util.ScreenShotHelper";
	}
	
	@Override
	public String getMethodMapping(String original) {
		return null;
	}
	
	@Override
	public String toString() {
		return getClass().getSimpleName() + "{" +
				"forgeClient=" + isForgeClient() +
				",helperClass=" + getHelperClassName() +
				'}';
	}
}
