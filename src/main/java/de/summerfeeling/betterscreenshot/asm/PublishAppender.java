package de.summerfeeling.betterscreenshot.asm;

import de.summerfeeling.betterscreenshot.BetterScreenshot;
import de.summerfeeling.betterscreenshot.publisher.Publisher;
import de.summerfeeling.betterscreenshot.publisher.Result;
import net.labymod.core.LabyModCore;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.event.ClickEvent;
import net.minecraft.event.HoverEvent;
import net.minecraft.event.HoverEvent.Action;
import net.minecraft.util.*;

import java.awt.*;
import java.awt.datatransfer.StringSelection;
import java.io.File;

public class PublishAppender {
	
	private static final BetterScreenshot ADDON = BetterScreenshot.getInstance();
	
	public static IChatComponent appendPublisher(IChatComponent component) {
		if (!BetterScreenshot.getInstance().isEnabled()) return component;
		
		ChatComponentTranslation translation = (ChatComponentTranslation) component;
		ChatComponentText displayMessage = (ChatComponentText) translation.getFormatArgs()[0];
		
		String fileName = displayMessage.getChatStyle().getChatClickEvent().getValue();
		String displayName = displayMessage.getUnformattedTextForChat();
		
		if (BetterScreenshot.getInstance().isDirectUpload()) {
			Publisher.publishScreenshot(new File(fileName), (result, detail) -> {
				EntityPlayerSP player = LabyModCore.getMinecraft().getPlayer();
				
				if (result == Result.UPLOAD_DONE) {
					player.addChatMessage(new ChatComponentText("§aUpload finished: " + detail).setChatStyle(new ChatStyle().setChatClickEvent(new ClickEvent(ClickEvent.Action.OPEN_URL, detail))));
					if (BetterScreenshot.getInstance().isCopyToClipboard()) Toolkit.getDefaultToolkit().getSystemClipboard().setContents(new StringSelection(detail), new StringSelection(detail));
				} else {
					player.addChatMessage(new ChatComponentText("§cUpload failed: §f" + detail));
				}
			});
			
			return new ChatComponentText("Saved screenshot as ").setChatStyle(new ChatStyle().setColor(EnumChatFormatting.GOLD))
					.appendSibling(new ChatComponentText(displayName).setChatStyle(new ChatStyle().setColor(EnumChatFormatting.GOLD).setUnderlined(true).setChatHoverEvent(new HoverEvent(Action.SHOW_TEXT, new ChatComponentText("Open screenshot"))).setChatClickEvent(new ClickEvent(ClickEvent.Action.OPEN_FILE, fileName))));
		}
		
		IChatComponent firstComponent = new ChatComponentText("Saved screenshot as ").setChatStyle(new ChatStyle().setColor(EnumChatFormatting.GOLD))
				.appendSibling(new ChatComponentText(displayName).setChatStyle(new ChatStyle().setColor(EnumChatFormatting.YELLOW).setChatClickEvent(new ClickEvent(ClickEvent.Action.OPEN_FILE, fileName)).setChatHoverEvent(new HoverEvent(Action.SHOW_TEXT, new ChatComponentText("Open screenshot")))));
		
		IChatComponent secondComponent = new ChatComponentText("\n» ").setChatStyle(new ChatStyle().setColor(EnumChatFormatting.GRAY));
		
		if (ADDON.isUploadEnabled()) secondComponent.appendSibling(new ChatComponentText("[UPLOAD] ").setChatStyle(new ChatStyle().setColor(EnumChatFormatting.YELLOW).setBold(true).setChatHoverEvent(new HoverEvent(Action.SHOW_TEXT, new ChatComponentText("Upload to imgur"))).setChatClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "-uploadscreenshot \"" + fileName + "\""))));
		if (ADDON.isDeleteEnabled()) secondComponent.appendSibling(new ChatComponentText("[DELETE]").setChatStyle(new ChatStyle().setColor(EnumChatFormatting.RED).setBold(true).setChatHoverEvent(new HoverEvent(Action.SHOW_TEXT, new ChatComponentText("Delete screenshot"))).setChatClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "-deletescreenshot \"" + fileName + "\""))));
		
		if (ADDON.isUploadEnabled() || ADDON.isDeleteEnabled()) firstComponent.appendSibling(secondComponent);
		
		return firstComponent;
	}
	
}
