package de.summerfeeling.betterscreenshot.asm;

import de.summerfeeling.betterscreenshot.BetterScreenshot;
import de.summerfeeling.betterscreenshot.ChatHelper;
import de.summerfeeling.betterscreenshot.fastshot.TakeScreenshotTask;
import net.labymod.support.util.Debug;
import net.labymod.support.util.Debug.EnumDebugMode;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.OpenGlHelper;
import net.minecraft.client.renderer.texture.TextureUtil;
import net.minecraft.client.shader.Framebuffer;
import net.minecraft.event.ClickEvent;
import net.minecraft.event.ClickEvent.Action;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.ChatComponentTranslation;
import net.minecraft.util.IChatComponent;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import org.apache.logging.log4j.LogManager;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.nio.IntBuffer;
import java.util.Date;

import static de.summerfeeling.betterscreenshot.BetterScreenshot.DATE_FORMAT;
import static de.summerfeeling.betterscreenshot.BetterScreenshot.SCREENSHOT_FOLDER;

@SideOnly(Side.CLIENT)
public class CustomScreenshotHelper {
	
	private static IntBuffer pixelBuffer;
	private static int[] pixelValues;
	
	static {
		Debug.log(EnumDebugMode.ASM, "[BetterScreenshot] Using data dir: " + Minecraft.getMinecraft().mcDataDir);
	}
	
	/**
	 * Saves a screenshot in the game directory with a time-stamped filename.
	 */
	public static IChatComponent saveScreenshot(File gameDirectory, int width, int height, Framebuffer buffer) {
		return saveScreenshot(gameDirectory, null, width, height, buffer);
	}
	
	/**
	 * Saves a screenshoit in the game directory with the given file name (or null to generate a time-stamped name).
	 */
	public static IChatComponent saveScreenshot(File gameDirectory, String screenshotName, int width, int height, Framebuffer buffer) {
		try {
			boolean bufferEnabled = OpenGlHelper.isFramebufferEnabled();
			
			if (bufferEnabled) {
				width = buffer.framebufferTextureWidth;
				height = buffer.framebufferTextureHeight;
			}

			int size = width * height;
			
			if (pixelBuffer == null || pixelBuffer.capacity() < size) {
				pixelBuffer = BufferUtils.createIntBuffer(size);
				pixelValues = new int[size];
			}
			
			GL11.glPixelStorei(GL11.GL_PACK_ALIGNMENT, 1);
			GL11.glPixelStorei(GL11.GL_UNPACK_ALIGNMENT, 1);
			pixelBuffer.clear();
			
			if (bufferEnabled) {
				GlStateManager.bindTexture(buffer.framebufferTexture);
				GL11.glGetTexImage(GL11.GL_TEXTURE_2D, 0, GL12.GL_BGRA, GL12.GL_UNSIGNED_INT_8_8_8_8_REV, pixelBuffer);
			}
			
			pixelBuffer.get(pixelValues);
			TextureUtil.processPixelValues(pixelValues, width, height);
			
			if (BetterScreenshot.getInstance().isFastScreenshots()) {
				int finalWidth = width;
				int finalHeight = height;
				BetterScreenshot.ASYNC_POOL.execute(() -> new TakeScreenshotTask(buffer, pixelBuffer, pixelValues, bufferEnabled, finalWidth, finalHeight).run());
				return ChatHelper.deleteMessage(new ChatComponentText("§d§d§d§d§d§1§2§3"));
			} else {
				BufferedImage image = null;
	
				if (bufferEnabled) {
					image = new BufferedImage(buffer.framebufferWidth, buffer.framebufferHeight, 1);
					int heightDiff = buffer.framebufferTextureHeight - buffer.framebufferHeight;
	
					for (int h = heightDiff; h < buffer.framebufferTextureHeight; ++h) {
						for (int w = 0; w < buffer.framebufferWidth; ++w) {
							image.setRGB(w, h - heightDiff, pixelValues[h * buffer.framebufferTextureWidth + w]);
						}
					}
				} else {
					image = new BufferedImage(width, height, 1);
					image.setRGB(0, 0, width, height, pixelValues, 0, width);
				}
	
				File outputFile = screenshotName == null ? getTimestampedPNGFileForDirectory(SCREENSHOT_FOLDER) : new File(SCREENSHOT_FOLDER, screenshotName);
				ImageIO.write(image, "png", outputFile);
	
				IChatComponent filePath = new ChatComponentText(outputFile.getName());
				filePath.getChatStyle().setChatClickEvent(new ClickEvent(Action.OPEN_FILE, outputFile.getAbsolutePath()));
				filePath.getChatStyle().setUnderlined(true);
	
				return PublishAppender.appendPublisher(new ChatComponentTranslation("screenshot.success", filePath));
			}
		} catch (Exception e) {
			LogManager.getLogger().warn("Could't save screenshot", e);
			return new ChatComponentTranslation("screenshot.failure", e.getMessage());
		}
	}
	
	public static File getTimestampedPNGFileForDirectory(File gameDirectory) {
		String date = DATE_FORMAT.format(new Date());
		int i = 1;
		
		while (true) {
			File file1 = new File(gameDirectory, date + (i == 1 ? "" : "_" + i) + ".png");
			if (!file1.exists()) return file1;
			++i;
		}
	}
	
}