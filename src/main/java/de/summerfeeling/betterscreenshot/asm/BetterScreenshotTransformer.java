package de.summerfeeling.betterscreenshot.asm;

import de.summerfeeling.betterscreenshot.asm.implementation.MC18Implementation;
import de.summerfeeling.betterscreenshot.asm.implementation.MappingAdapter;
import de.summerfeeling.betterscreenshot.asm.implementation.UnobfuscatedImplementation;
import net.labymod.core.asm.LabyModCoreMod;
import net.labymod.main.Source;
import net.labymod.support.util.Debug;
import net.labymod.support.util.Debug.EnumDebugMode;
import net.minecraft.launchwrapper.IClassTransformer;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.MethodNode;

import java.io.IOException;

public class BetterScreenshotTransformer implements IClassTransformer {
	
	private MappingAdapter implementation;
	
	public BetterScreenshotTransformer() {
		if (System.getProperty("bsdebug") != null && System.getProperty("bsdebug").equals("true")) {
			this.implementation = new UnobfuscatedImplementation(true);
			Debug.log(EnumDebugMode.ASM, "[BetterScreenshot] Using " + implementation.toString() + " mapping implementation");
			return;
		}
		
		if (Source.ABOUT_MC_VERSION.startsWith("1.8")) {
			this.implementation = new MC18Implementation(LabyModCoreMod.isForge());
		} else {
			System.err.println("[BetterScreenshot] This addon does not support your minecraft version " + Source.ABOUT_MC_VERSION + "!");
		}
		
		if (implementation != null) Debug.log(EnumDebugMode.ASM, "[BetterScreenshot] Using " + implementation.toString() + " mapping implementation");
	}
	
	public byte[] transform(String name, String transformedName, byte[] basicClass) {
		if (implementation == null) return basicClass;
		
		if (transformedName.equals(implementation.getHelperClassName())) {
			Debug.log(EnumDebugMode.ASM, "[BetterScreenshot] Transforming " + implementation.getHelperClassName().replace(".", "/") + " to de/summerfeeling/betterscreenshot/asm/CustomScreenshotHelper");
			
			try {
				ClassReader reader = new ClassReader(getClass().getClassLoader().getResourceAsStream("de/summerfeeling/betterscreenshot/asm/CustomScreenshotHelper.class"));
				ClassNode node = new ClassNode();
				reader.accept(node, 0);
				
				node.visit(node.version, node.access, implementation.getHelperClassName().replace(".", "/"), node.signature, node.superName, node.interfaces.toArray(new String[0]));
				
				for (MethodNode methods : node.methods) {
					String mapping = implementation.getMethodMapping(methods.name + "§" + methods.desc);
					if (mapping != null) methods.name = mapping;
				}
				
				ClassWriter writer = new ClassWriter(ClassWriter.COMPUTE_FRAMES | ClassWriter.COMPUTE_MAXS);
				node.accept(writer);
				
				return writer.toByteArray();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		return basicClass;
	}
}
