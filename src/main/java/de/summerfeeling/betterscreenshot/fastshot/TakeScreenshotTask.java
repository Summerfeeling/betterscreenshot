package de.summerfeeling.betterscreenshot.fastshot;

import de.summerfeeling.betterscreenshot.asm.PublishAppender;
import net.minecraft.client.Minecraft;
import net.minecraft.client.shader.Framebuffer;
import net.minecraft.event.ClickEvent;
import net.minecraft.event.ClickEvent.Action;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.ChatComponentTranslation;
import net.minecraft.util.IChatComponent;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.nio.IntBuffer;

import static de.summerfeeling.betterscreenshot.BetterScreenshot.SCREENSHOT_FOLDER;
import static de.summerfeeling.betterscreenshot.asm.CustomScreenshotHelper.getTimestampedPNGFileForDirectory;

public class TakeScreenshotTask {
	
	private boolean bufferEnabled;
	private IntBuffer pixelBuffer;
	private BufferedImage image;
	private Framebuffer buffer;
	private int[] pixelValues;
	
	private int height;
	private int width;
	
	public TakeScreenshotTask(Framebuffer framebuffer, IntBuffer pixelBuffer, int[] pixelValues, boolean bufferEnabled, int width, int height) {
		this.bufferEnabled = bufferEnabled;
		this.pixelBuffer = pixelBuffer;
		this.pixelValues = pixelValues;
		this.buffer = framebuffer;
	
		this.height = height;
		this.width = width;
	}

	@SuppressWarnings("Duplicates")
	public void run() {
		try {
			Minecraft.getMinecraft().thePlayer.addChatMessage(new ChatComponentText("§a§oTaking..."));
			long start = System.currentTimeMillis();
			
			if (bufferEnabled) {
				image = new BufferedImage(buffer.framebufferWidth, buffer.framebufferHeight, 1);
				int heightDiff = buffer.framebufferTextureHeight - buffer.framebufferHeight;
				
				for (int h = heightDiff; h < buffer.framebufferTextureHeight; ++h) {
					for (int w = 0; w < buffer.framebufferWidth; ++w) {
						image.setRGB(w, h - heightDiff, pixelValues[h * buffer.framebufferTextureWidth + w]);
					}
				}
			} else {
				image = new BufferedImage(width, height, 1);
				image.setRGB(0, 0, width, height, pixelValues, 0, width);
			}
			
			File outputFile = getTimestampedPNGFileForDirectory(SCREENSHOT_FOLDER);
			ImageIO.write(image, "png", outputFile);
			
			IChatComponent filePath = new ChatComponentText(outputFile.getName());
			filePath.getChatStyle().setChatClickEvent(new ClickEvent(Action.OPEN_FILE, outputFile.getAbsolutePath()));
			filePath.getChatStyle().setUnderlined(true);
			
			Minecraft.getMinecraft().addScheduledTask(() -> {
				Minecraft.getMinecraft().thePlayer.addChatMessage(PublishAppender.appendPublisher(new ChatComponentTranslation("screenshot.success", filePath)));
				Minecraft.getMinecraft().thePlayer.addChatMessage(new ChatComponentText("§6§oTook " + (System.currentTimeMillis() - start) + "ms"));
				return null;
			});
		} catch (Exception e) {
			Minecraft.getMinecraft().addScheduledTask(() -> {
				Minecraft.getMinecraft().thePlayer.addChatMessage(new ChatComponentTranslation("screenshot.failure", e.getMessage()));
				return null;
			});
		}
	}
}
