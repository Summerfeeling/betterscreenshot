package de.summerfeeling.betterscreenshot.utils;

import de.summerfeeling.betterscreenshot.ButtonElement;

public class DescribableButtonElement extends ButtonElement {
	
	public DescribableButtonElement(String displayName, IconData iconData, String buttonText) {
		super(displayName, iconData, buttonText);
	}
	
	public DescribableButtonElement(String displayName, IconData iconData, String buttonText, Runnable toggleListener) {
		super(displayName, iconData, buttonText, toggleListener);
	}
	
	public ButtonElement editDescriptionText(String descriptionText) {
		super.setDescriptionText(descriptionText);
		return this;
	}
}
