package de.summerfeeling.betterscreenshot;

import net.labymod.ingamechat.renderer.ChatLine;
import net.labymod.main.LabyMod;
import net.labymod.utils.ModColor;
import net.minecraft.client.Minecraft;
import net.minecraft.util.IChatComponent;

import java.io.File;
import java.util.*;

public class ChatHelper {
	
	public static IChatComponent deleteMessage(IChatComponent component) {
		new Timer().schedule(new TimerTask() {
			public void run() {
				Minecraft.getMinecraft().addScheduledTask(() -> {
					LabyMod.getInstance().getIngameChatManager().getMain().getChatLines().removeIf(line -> line.getMessage().contains("§d§d§d§d§d§1§2§3"));
					return null;
				});
			}
		}, 50L);
		return component;
	}
	
	public static void deleteMessage(File file) {
		List<ChatLine> lines = LabyMod.getInstance().getIngameChatManager().getMain().getChatLines();
		Iterator<ChatLine> iterator = lines.iterator();
		
		ChatLine previous = null;
		int index = 0;
		
		while (iterator.hasNext()) {
			ChatLine fileLine = iterator.next();
			String message = ModColor.removeColor(fileLine.getMessage());
			
			if (message.startsWith("Saved screenshot") && message.endsWith(file.getName())) {
				iterator.remove();
				
				if (previous != null) {
					String actionMessage = ModColor.removeColor(previous.getMessage());
				
					if (actionMessage.startsWith("» [UPLOAD]")) {
						LabyMod.getInstance().getIngameChatManager().getMain().getChatLines().remove(index);
					}
				}
				
				break;
			}
			
			if (previous != null) index++;
			previous = fileLine;
		}
	}

}
